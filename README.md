## KAYU - Milis Linux Kullanıcı Arayüz Üretici Uygulaması

Kayu, arkaplanda [gtkdialog](https://github.com/puppylinux-woof-CE/gtkdialog) uygulamasını kullanarak basit arayüz uygulamaları geliştirmeye yarayan arayüz üretme uygulamasıdır.
Arayüz için gerekli bileşenler ve aksiyonlar INI biçimindeki dosyada tanımlanarak kayu ile çalıştırılır. Kayu, INI tanımlamalarını XML biçimine çevirerek gtkdialog ile çalıştırır.
INI biçimi kullanılmasının nedeni, daha kolay ve anlaşılır bir şekilde arayüz tanımlamalarının yapılabilmesidir.
Ayrıntılar için örnekler dizini incelenebilir.

### Arayüz XML Yazdırma
```
./kayu.py ornekler/uygulama.ini
```

### Arayüz Çalıştırma
```
./kayu.py ornekler/uygulama.ini r
```

### Ağdan Çalıştırma
```
curl https://0x0.st/HtxY.ini | ./kayu.py | gtkdialog -s
```


