### Gtkdiaolog - Kayu Arayüz Elemanları Tablosu

  

Aşağıda [Gtkdiaolog](https://github.com/puppylinux-woof-CE/gtkdialog/wiki) da kullanılan Kayu karşılık gelen arayüz elemanları yer almaktadır.

  

<table border=1>
<th>gtkdialog</th><th>kayu</th>
<tr>
<td>button</td>
<td>button</td>
</tr>
<tr>
<td>checkbox </td>
<td>check</td>
</tr>
<tr>
<td>chooser </td>
<td>filebox</td>
</tr>
<tr>
<td>colorbutton </td>
<td>color</td>
</tr>
<tr>
<td>combobox </td>
<td>-</td>
</tr>
<tr>
<td>comboboxentry</td>
<td>combo</td>
</tr>
<tr>
<td>comboboxtext</td>
<td>-</td>
</tr>
<tr>
<td>edit </td>
<td>editor</td>
</tr>
<tr>
<td>entry </td>
<td>input</td>
</tr>
<tr>
<td>eventbox </td>
<td>box</td>
</tr>
<tr>
<td>expander </td>
<td>tree</td>
</tr>
<tr>
<td>fontbutton </td>
<td>-</td>
</tr>
<tr>
<td>frame </td>
<td>frame</td>
</tr>
<tr><td>hbox</td>
<td>row</td></tr>
<tr>
<td>hscale </td>
<td>-</td>
</tr>
<tr>
<td>hseparator </td>
<td>line</td>
</tr>
<tr>
<td>list </td>
<td>-</td>
</tr>
<tr>
<td>menu </td>
<td>menu</td>
</tr>
<tr>
<td>menubar </td>
<td>bar</td>
</tr>
<tr>
<td>menuitem </td>
<td>item</td>
</tr>
<tr>
<td>menuitemseparator </td>
<td>sep</td>
</tr>
<tr>
<td>notebook </td>
<td>pager</td>
</tr>
<tr>
<td>pixmap </td>
<td>image</td>
</tr>
<tr>
<td>progressbar </td>
<td>progress</td>
</tr>
<tr>
<td>radiobutton </td>
<td>radio</td>
</tr>
<tr>
<td>separator </td>
<td>-</td>
</tr>
<tr>
<td>spinbutton </td>
<td>spin</td>
</tr>
<tr>
<td>statusbar </td>
<td>status</td>
</tr>
<tr>
<td>table </td>
<td>-</td>
</tr>
<tr>
<td>terminal </td>
<td>term</td>
</tr>
<tr>
<td>text </td>
<td>label</td>
</tr>
<tr>
<td>timer </td>
<td>timer</td>
</tr>
<tr>
<td>togglebutton </td>
<td>toggle</td>
</tr>
<tr>
<td>tree</td>
<td>table</td>
</tr>
<tr><td>vbox</td>
<td>colon</td>
</tr>
<tr><td>vscale</td>
<td>-</td>
</tr>
<tr><td>vseparator</td>
<td>bracket</td>
</tr>
<tr><td>window</td>
<td>window</td>
</tr>
</table>
