#!/bin/env python3

# gtkdialog proxy script with ini formatted user interface

import sys
import configparser
import fileinput

config = configparser.ConfigParser()

ui_file = ""

mod = None

# mod okuma
if len(sys.argv) > 2:
  mod = sys.argv[2]

# tek dosya parametre
if len(sys.argv) > 1:
  ui_file = sys.argv[1]
  config.read(ui_file)

# stdin
if len(sys.argv)== 1:
  ini_cnt = ""
  for line in fileinput.input():
    ini_cnt +=line
  config.read_string(ini_cnt)

xmls=""

attrs = {
  "hint"  : "tooltip-text",
  "icon"  : "icon-name",
  "title" : "title",
  "if"    : "condition",
  "type"  : "signal",
  "fun"   : "function",
  "layer" : "layer",
  "edge"  : "edge",
  #"when"  : "when", #replace with signal
}

subs = {
  "cmd"    : ["input"],
  "stock"  : ["input file stock","input"],
  "icon"   : ["input file icon","input"],
  "source" : ["input file","input"],
  "output" : ["output file","output"],
  "var"    : ["variable"],
  "text"   : ["label"],
  "input-text"  : ["default"],
  "combo-text"  : ["default"],
  "path"   : ["default"],
  "header" : ["label"],
  "item"   : ["item"],
  
  "window" : "window",
  "colon"  : "vbox",
  "row"    : "hbox",
  "button" : "button",
  "editor" : "edit",
  "check"  : "checkbox",
  "radio"  : "radiobutton",
  "label"  : "text",
  "input"  : "entry",
  "action" : "action",
  "filebox": "chooser",
  "line"   : "hseparator",
  "table"  : "tree",
  "combo"  : "comboboxentry",
}

def h_true(config):
  str = ""
  if "true" in config:
   vals = config["true"].split(",")
   for val in vals:
     str+= f' {val}=\"true\" '
  return str

def h_var(config):
  var = h_sub({"var": config.name.replace("-","") + "var" },"var")
  if "var" in config:
    var = h_sub(config,"var")
  return var

def h_size(config):
  w = ""
  h = ""
  
  if "size" in config:
    w, h = config["size"].split(",") 
    if "window" in config.name:
      w = f' width-request="{w}" '
      h = f' height-request="{h}" '
    else:
      w = f'<width>"{w}"<width>'
      h = f'<height>"{h}"<height>'
  return w,h

def h_sub(config, sub):
  match = sub
  if not isinstance(config,dict):
    if "input" in config.name:
      match = "input-" + sub
    if "combo" in config.name and sub == "text":
      match = "combo-" + sub
  # subs eşleşen - aynı sub ile farklı eşleşme olabilir
  # text-label , text-default gibi
  if match not in subs: return ""
  subo = subs[match][0]
  subc = subs[match][0] if len(subs[match]) == 1 else subs[match][1]
  # config ile gelen sub
  if sub not in config: return ""
  if sub in ["icon","stock"]:
    return f'<{subo}="{config[sub]}"></{subc}>'
  return f'<{subo}>{config[sub]}</{subc}>'

def h_attr(config, attr):
  if attr not in config: return ""
  return f' {attrs[attr]}="{config[attr]}"'

def h_items(config,key):
  if key not in config: return ""
  items = ""
  for item in config["items"].split(","):
    items += f'<item>{item}</item>'
  return items

def h_window(config):
  w, h  = h_size(config)
  title = h_attr(config, "title")
  icon  = h_attr(config, "icon")
  layer = h_attr(config, "layer")
  edge  = h_attr(config, "edge")
  return f'<window{layer}{edge}{w}{h}{title}{icon}>'

def h_button(config):
  hint   = h_attr(config,"hint")
  source = h_sub(config,"source")
  stock  = h_sub(config,"stock")
  icon   = h_sub(config,"icon")
  var    = h_var(config) 
  text   = h_sub(config,"text")
  tip    = " "+config["type"] if ("type" in config and config["type"] in ["ok","cancel"]) else ""
  return f'<button{tip}{hint}>{stock}{var}{text}{icon}{source}'

def h_label(config):
  propt  = h_true(config)
  cmd    = h_sub(config,"cmd")
  source = h_sub(config,"source")
  var    = h_var(config) 
  text   = h_sub(config,"text")  
  return f'<text{propt}>{source}{var}{text}{cmd}'

def h_section(config):
  if "-" in config.name:
    sec = config.name.split("-")[0]
  xadd(f'<{subs[sec]}>')

def h_check(config):
  propt  = h_true(config)
  cmd    = h_sub(config,"cmd")
  source = h_sub(config,"source")
  var    = h_var(config) 
  text   = h_sub(config,"text")  
  return f'<checkbox{propt}>{source}{var}{text}{cmd}'

def h_action(config):
  # when = h_attr(config, "when") #replace with signal
  cond = h_attr(config, "if")
  tip  = h_attr(config, "type")
  fun  = h_attr(config, "fun")
  cmd  = config["cmd"] if "cmd" in config else ""
  
  return f'<action{tip}{cond}{fun}>{cmd}'

def h_input(config):
  propt  = h_true(config)
  cmd    = h_sub(config,"cmd")
  source = h_sub(config,"source")
  var    = h_var(config) 
  text   = h_sub(config,"text")
  return f'<entry{propt}>{source}{var}{text}{cmd}'

def h_editor(config):
  propt  = h_true(config)
  source = h_sub(config,"source")
  var    = h_var(config) 
  output = h_sub(config,"output")  
  return f'<edit{propt}>{source}{var}{output}'

def h_filebox(config):
  var    = h_var(config)
  w,h    = h_size(config)
  cmd    = h_sub(config,"cmd") 
  source = h_sub(config,"source")  
  output = h_sub(config,"output")
  path   = h_sub(config,"path")
  return f'<chooser action="1">{source}{var}{output}{cmd}{w}{h}{path}'

def h_table(config):
  propt  = h_true(config)
  cmd    = h_sub(config,"cmd")
  source = h_sub(config,"source")
  header = h_sub(config,"header")
  item   = h_sub(config,"item")
  var    = h_var(config) 
  return f'<tree{propt}>{source}{var}{header}{cmd}{item}'
  
def h_combo(config):
  propt  = h_true(config)
  cmd    = h_sub(config,"cmd")
  source = h_sub(config,"source")
  text   = h_sub(config,"text")
  items  = h_items(config,"items")
  var    = h_var(config)
  return f'<comboboxentry{propt}>{source}{var}{text}{cmd}{items}'

def xadd(strx):
  global xmls
  if strx == ">" and xmls[-1] == ">":
    strx = ""
  xmls += str(strx)

def handle(sec):
  if   "window" in sec: xadd(h_window(config[sec]))
  elif "button" in sec: xadd(h_button(config[sec]))
  elif "label"  in sec: xadd(h_label(config[sec]))
  elif "input"  in sec: xadd(h_input(config[sec]))
  elif "action" in sec: xadd(h_action(config[sec]))
  elif "colon"  in sec: h_section(config[sec])
  elif "row"    in sec: h_section(config[sec])
  elif "editor" in sec: xadd(h_editor(config[sec]))
  elif "check"  in sec: xadd(h_check(config[sec]))
  elif "filebox" in sec: xadd(h_filebox(config[sec]))
  elif "table"  in sec: xadd(h_table(config[sec]))
  elif "combo"  in sec: xadd(h_combo(config[sec]))
  elif "line"   in sec: h_section(config[sec])

def endup(sec):
  if "-" in sec:
    sec = sec.split("-")[0]
  xadd(f'</{subs[sec]}>\n')

def walk(sec):
  handle(sec)
  c = 0
  i = 0
  for key in config[sec]:
    if key == "1": break
    else: c+=1
  for key in config[sec]:
    i+=1
    if key.isnumeric():
      walk(config[sec][key])
  endup(sec)

walk("window")

# ince ayarlar
# eğer vbox yoksa boş ekle
if "vbox" not in xmls and "</hbox></window>" not in xmls:
  xmls = xmls.replace(">","><vbox>",1)
  xmls = xmls.replace("</window>","</vbox></window>")
# çalıştırma modu
if mod:
  from subprocess import Popen, PIPE, STDOUT
  gtk = Popen(['gtkdialog', '-s'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
  out = gtk.communicate(input=xmls.encode())[0]
  for x in out.decode().split("\n"):
    print("**",x)
# yazdırma modu
else:
  print(xmls)
